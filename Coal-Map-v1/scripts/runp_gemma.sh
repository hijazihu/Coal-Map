#Copyright (C) 2015 Hussein Hejase

#Coal-Map is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

#You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
numGeneTrees=$1
~hijazihu/gemma-0.94/bin/gemma -g data/GEMMA/geno.txt -p input/phenotype.txt -maf 0 -a data/GEMMA/annot.txt -gk -o kinship
for j in `seq 1 $numGeneTrees`;
do
 	~hijazihu/gemma-0.94/bin/gemma -g data/GEMMA/geno_$j.txt -p input/phenotype.txt -n 1 -c data/EIGS/example_combined_global_$j.txt -maf 0 -r2 1 -lmax 0.00001 -a data/GEMMA/annot_$j.txt -k output/kinship.cXX.txt -lmm 2 -o stat_lmm_$j		
	awk '{print $10}' output/stat_lmm_$j.assoc.txt > output/pval_$j.txt
       	awk '{print $11}' output/stat_lmm_$j.assoc.txt > output/lscore_$j.txt
done
rm output/kinship*
mv output/ data/par
