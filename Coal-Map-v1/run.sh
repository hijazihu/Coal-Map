#!/bin/bash
seq_file="$1"
meta_file="$2"
sample_file="$3"
breakpoints_file="$4"
phenotype_file="$5"

rm -r data
mkdir data
mkdir data/EIGS
mkdir data/GEMMA

Rscript scripts/read_gen.R $seq_file $meta_file $sample_file $breakpoints_file $phenotype_file
numGeneTrees=$(wc -l input/$breakpoints_file)
numGeneTrees=$(echo $numGeneTrees|cut -d' ' -f1)

perl scripts/run_par.perl $numGeneTrees
perl scripts/run_con.perl 

sh scripts/parse_pc.sh $numGeneTrees

sh scripts/run_gemma.sh
sh scripts/runp_gemma.sh $numGeneTrees

Rscript scripts/write_pval.R $numGeneTrees $meta_file
