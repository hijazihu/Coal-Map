Title: Coal-Map
Authors: Hussein E. Hejase & Kevin J. Liu

LICENSE: Coal-Map  is distributed under the terms of the GNU General Public License as published by the Free Software Foundation. You can distribute or modify it under the terms of the GNU General Public License either version 3 of the License or any later version.

To run Coal-Map, you need to install the following:
EIGENSTART: http://genetics.med.harvard.edu/reich/Reich_Lab/Software.html
GEMMA: http://www.xzlab.org/software.html
LAPACK package: http://www.netlib.org/lapack/
The GNU Scientific Library (GSL): http://www.gnu.org/software/gsl/
The latest R version (v3.2.0)

The following R packages are need to run Coal-Map:
seqin
qqman

How to run Coal-Map?
sh run.sh <fasta file> <metadata> <phased> <breakpoints>

<fasta file> : FASTA-formatted multiple sequence alignment file

<metadata> :  A space delimited file containing one entry per locus
in ascending genomic coordinate order formatted as follow:
<chromosome> <genomic coordinate of locus> <major allele> <minor allele>

<taxon> : A file containing taxon names (one taxa name per row)
 
breakpoints: A file containing the breakpoints (one set of breakpoints per row) formatted as follow:
Each row contains two breakpoints - left and right end points of a partition 
e.g.,
1 10
11 100
101 200
201 300

An example dataset is located in the example_dataset folder.

Run Coal-Map using the following command:
sh run.sh chr7_seq.fasta chr7_metadata.ssv phased.fasta.map breakpoints.txt phenotype.txt
where all the required files should be located in the "input" folder.

The output is located in the file: result.txt, which contains the snp id, chromosome, position, and p-value of each locus.

